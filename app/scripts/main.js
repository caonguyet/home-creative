
$(document).ready(function () {
  $("#owl-slider").owlCarousel({
    navigation: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true,
    // items: 1,

    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },

    itemsDesktop: true,
    itemsDesktopSmall: true,
    itemsTablet: true,
    itemsMobile: true,
  });
});


// Navbar

let mainNav = document.getElementById('js-menu');
let navToggle =  document.getElementById('js-navbar-toggle');
navToggle.addEventListener('click', function(){
    mainNav.classList.toggle('active')
})

//menu sticky

window.onscroll = function() {menuSticky()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function menuSticky() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

